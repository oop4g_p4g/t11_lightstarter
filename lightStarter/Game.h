#ifndef GAME_H
#define GAME_H

#include <vector>

#include "Mesh.h"
#include "Model.h"
#include "Singleton.h"

//forward declaration, we don't need the full definition if only using a pointer/reference
class MyD3D;

/*
* a simple scene manager singleton
* render some boxes and lights and do some simple kinematics
*/
class Game : public Singleton<Game>
{
public:
	~Game() {
		Release();
	}
	void Update(float dTime);
	void Render(float dTime);
	void Initialise();
	void Release();
	LRESULT WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	//control where the camera is and track it as it moves around
	const DirectX::SimpleMath::Vector3 mDefCamPos = DirectX::SimpleMath::Vector3(0, 2, -5);
	DirectX::SimpleMath::Vector3 mCamPos = DirectX::SimpleMath::Vector3(0, 2, -5);

	//some simple objects within our scene
	Model mBox, mQuad, mLight;

private:

	//create some meshes and attach models to them
	void BuildCube();
	void BuildQuad();

	//spin things around
	float mAngle = 0;
};


#endif

