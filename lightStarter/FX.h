#ifndef FX_H
#define FX_H

#include <string>
#include <d3d11.h>
#include "ShaderTypes.h"
#include "D3DUtil.h"

class Model;
class MyD3D;

namespace FX
{
	DirectX::SimpleMath::Matrix& GetProjectionMatrix();
	DirectX::SimpleMath::Matrix& GetViewMatrix();

	/*we've loaded in a "blob" of compiled shader code, it needs to be set up on the gpu as a pixel shader
	* d3dDevice - IN the gpu device to hold this pixel shader
	* pBuff - IN compiled program code
	* buffSz - IN how big the program code is
	* pPS - OUT a pointer reference to a D3D pixel shader object
	*/
	void CreatePixelShader(ID3D11Device& d3dDevice, char* pBuff, unsigned int buffSz, ID3D11PixelShader* &pPS);
	/*we've loaded in a "blob" of compiled shader code, it needs to be set up on the gpu as a vertex shader
	* d3ddevice
	* pBuff - IN compiled program code
	* buffSz - IN how big the program code is
	* pVS - OUT a pointer reference to a D3D vertex shader object
	*/
	void CreateVertexShader(ID3D11Device& d3dDevice, char* pBuff, unsigned int buffSz, ID3D11VertexShader* &pVS);
	/*the input assembler needs to know what is in the vertex buffer, how to get the data outand which vertex shader to give it to
	* d3dDevice
	* vdesc - IN a vertex description
	* numElements - IN how many items are in the vertex description
	* pBuff - IN compiled program code this input layout matches (vertex shader)
	* buffSz - IN how big the program code is
	* pLayout - OUT pointer reference to a D3D input layout object
	*/
	void CreateInputLayout(ID3D11Device& d3dDevice, const D3D11_INPUT_ELEMENT_DESC vdesc[], int numElements, char* pBuff, unsigned int buffSz, ID3D11InputLayout** pLayout);
	/*
	* A constant buffer object can pass constants to a gpu
	* d3dDevice
	* sizeOfBuffer - IN usually this is the size of some struct containing constants to shift to the GPU
	* pBuffer - OUT a d3d object that can hold the constant data we will eventually be passing over
	*/
	void CreateConstantBuffer(ID3D11Device& d3dDevice, UINT sizeOfBuffer, ID3D11Buffer **pBuffer);
	//any PC within the last 5+ years should be shader model 5 compatible, if not then it's too old
	void CheckShaderModel5Supported(ID3D11Device& d3dDevice);
	/*
	* Read data from a file, usually used to load in compiled shader code
	* fileName - IN file to load
	* bytesRead - OUT how much data we got
	*/
	char* ReadAndAllocate(const std::string& fileName, unsigned int& bytesRead);

	//create all constant buffers needed once
	void CreateConstantBuffers(MyD3D& d3d);
	//get rid of them once at the end
	void ReleaseConstantBuffers();
	//update the gpu prior to rendering something - rebuild the master transform
	//ctx - d3d device context
	//worl - reference to a local->world space matrix to use when rebuilding the master transform
	void SetPerObjConsts(ID3D11DeviceContext& ctx, DirectX::SimpleMath::Matrix &world);
	//update the gpu at the start of a new render udpate (once per frame)
	//ctx - device context
	//eyePos - where is the camera? Assume it only moves once per frame
	void SetPerFrameConsts(ID3D11DeviceContext& ctx, const DirectX::SimpleMath::Vector3& eyePos);
	/*called before something is rendered
	* ctx - IN device context
	* mat - IN a material with info on textures, render parameters, reflectance values
	*/
	void PreRenderObj(ID3D11DeviceContext& ctx, Material& mat);
	/*
	* A directional light - like the sun
	* lightIdx - IN we pass an array of light info to the gpu, which array element is this one using
	* enable - IN true if we are turning it on
	* direction - which way is it pointing
	* diffuse - colour intensity of the light
	* specular - specular intensity
	* ambient - any ambient light that is meant to leak into the scene
	*/
	void SetupDirectionalLight(int lightIdx, bool enable,
		const DirectX::SimpleMath::Vector3 &direction,
		const DirectX::SimpleMath::Vector3& diffuse = DirectX::SimpleMath::Vector3(1, 1, 1),
		const DirectX::SimpleMath::Vector3& specular = DirectX::SimpleMath::Vector3(0, 0, 0),
		const DirectX::SimpleMath::Vector3& ambient = DirectX::SimpleMath::Vector3(0, 0, 0));
	/*
	* Same as above, but this is a point light, so it shines in all directions
	* position - IN where the point light is in the world
	* range - IN after this far stop lighting things
	* atten1 - IN control teh rate of change of intensity over distance
	*/
	void SetupPointLight(int lightIdx, bool enable,
		const DirectX::SimpleMath::Vector3 &position,
		const DirectX::SimpleMath::Vector3& diffuse = DirectX::SimpleMath::Vector3(1, 1, 1),
		const DirectX::SimpleMath::Vector3& specular = DirectX::SimpleMath::Vector3(0, 0, 0),
		const DirectX::SimpleMath::Vector3& ambient = DirectX::SimpleMath::Vector3(0, 0, 0),
		float range = 1000.f, float atten1 = 0.05f);
	/*
	* Same as above, but this is a spotlight, so it has a position and direction and shines out in a cone shape
	* innerConeTheta - IN radian angle of the inner cone, inside this cone intensity is constant
	* outerConePhi - IN radian angle of the outer cone, intensity falls off to this edge and then the light stops
	*/
	void SetupSpotLight(int lightIdx, bool enable,
		const DirectX::SimpleMath::Vector3 &position,
		const DirectX::SimpleMath::Vector3 &direction,
		const DirectX::SimpleMath::Vector3& diffuse = DirectX::SimpleMath::Vector3(1, 1, 1),
		const DirectX::SimpleMath::Vector3& specular = DirectX::SimpleMath::Vector3(0, 0, 0),
		const DirectX::SimpleMath::Vector3& ambient = DirectX::SimpleMath::Vector3(0, 0, 0),
		float range = 1000.f, float atten1 = 0.05f, float innerConeTheta = D2R(30), float outerConePhi = D2R(40));
	//start using bit flags to control rendering, currently we can just turn lighting on and off in a material
	typedef enum { NONE = 0, LIT = 1 } FxFlags;

	//an object to bring state related to shaders together
	class MyFX
	{
	public:
		MyFX(MyD3D& d3d)
			:mD3D(d3d) {}
		~MyFX() {
			Release();
		}
		//load shaders, create constant registers
		bool Init();
		//release all the shader related d3d objects
		void Release();
		/*
		* we don't render low level geometry anymore, we've wrapped that up in Models and Meshes
		* model - IN a model is an instance of a Mesh which holds geometry buffers
		* flags - IN bit flags controlling how a model is rendered
		* pOverrideMat - IN Models and Meshes have materials controlling how they look, but we can optionally
		*				pass one in to use instead, so we can make the model look different without altering it
		*/
		void Render(Model& model, unsigned int flags, Material* pOverrideMat = nullptr);


	private:
		ID3D11InputLayout* mpInputLayout = nullptr;					//vertex structure input layout
		ID3D11VertexShader* mpVS = nullptr;							//we only have one vertex shader program to keep it simple
		ID3D11PixelShader* mpPSLit = nullptr, *mpPSUnlit = nullptr;	//we have two pixel shaders, one simple and one that handles lighting
		MyD3D& mD3D;	//handle to the main D3D object
	};

}

#endif
