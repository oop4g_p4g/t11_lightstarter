#ifndef SHADERTYPES_H
#define SHADERTYPES_H

#include <d3d11.h>

#include "SimpleMath.h"

/*
This is what our vertex data will look like
*/
struct VertexPosNorm
{
	DirectX::SimpleMath::Vector3 Pos;	//local space position of the vertex
	DirectX::SimpleMath::Vector3 Norm;	//colour red, green, blue, alpha

	//a description of this structure that we can pass to d3d
	static const D3D11_INPUT_ELEMENT_DESC sVertexDesc[2];
};

/*
Instead of a colour in each vertex we define a material
for a group of primitves (an entire surface)
*/
struct Material
{
	//constructors
	Material(const DirectX::SimpleMath::Vector4& a, const DirectX::SimpleMath::Vector4& d, const DirectX::SimpleMath::Vector4& s) {
		Ambient = a;
		Diffuse = d;
		Specular = s;
	}
	Material() : Ambient(1, 1, 1, 1), Diffuse(1, 1, 1, 1), Specular(1, 1, 1, 1) {}

	//light reflection modulators
	DirectX::SimpleMath::Vector4 Ambient;
	DirectX::SimpleMath::Vector4 Diffuse;
	DirectX::SimpleMath::Vector4 Specular; // w = SpecPower
};

/*
* the definition of a light, could be on or off, could be directional (sun), spot or point
*/
struct Light
{
	Light() : type(OFF), range(0), theta(0), phi(0)
	{}

	DirectX::SimpleMath::Vector4 Ambient;
	DirectX::SimpleMath::Vector4 Diffuse;
	DirectX::SimpleMath::Vector4 Specular;		//w=power
	DirectX::SimpleMath::Vector4 Direction;		//w=nothing
	DirectX::SimpleMath::Vector4 Position;		//w=nothing
	DirectX::SimpleMath::Vector4 Attenuation;	//w=nothing

	typedef enum {
		OFF = 0,		//ignore this light
		DIR = 1,	//directional
		POINT = 2,	//point light
		SPOT = 3	//spot light
	} Type;
	int type;

	float range;	//a distance after which the light stops having any effect
	float theta;	//inner cone angle
	float phi;		//outer cone angle
};

//shader variables that don't change within one frame
const int MAX_LIGHTS = 3;
struct GfxParamsPerFrame
{
	Light lights[MAX_LIGHTS];				//all the light descriptions needed to render something
	DirectX::SimpleMath::Vector4 eyePosW;	//where the camera is
};
static_assert((sizeof(GfxParamsPerFrame) % 16) == 0, "CB size not padded correctly");



//shader variables that don't change within one object
struct GfxParamsPerObj
{
	DirectX::SimpleMath::Matrix world;			//local to world space matrix
	DirectX::SimpleMath::Matrix worldInvT;		//inverse transpose used for transforming normals
	DirectX::SimpleMath::Matrix worldViewProj;	//the master transform
};
static_assert((sizeof(GfxParamsPerObj) % 16) == 0, "CB size not padded correctly");


//shader variables that don't change within an object's sub-mesh
struct GfxParamsPerMesh
{
	Material material;	//a material defines soem geometries reflectance values
};
static_assert((sizeof(GfxParamsPerMesh) % 16) == 0, "CB size not padded correctly");

#endif
