#include <iostream>
#include <fstream>

#include "D3D.h"
#include "D3DUtil.h"
#include "FX.h"
#include "ShaderTypes.h"
#include "WindowUtils.h"
#include "Model.h"
#include "Mesh.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

namespace FX
{

	GfxParamsPerObj gGfxPerObj;
	GfxParamsPerFrame gGfxPerFrame;
	GfxParamsPerMesh gGfxPerMesh;
	ID3D11Buffer *gpGfxPerObj = nullptr, *gpGfxPerFrame = nullptr, *gpGfxPerMesh = nullptr;
	Matrix gView, gProj;

	DirectX::SimpleMath::Matrix& GetProjectionMatrix()
	{
		return gProj;
	}

	DirectX::SimpleMath::Matrix& GetViewMatrix()
	{
		return gView;
	}


	void CreateConstantBuffers(MyD3D& d3d)
	{
		ID3D11Device& d = d3d.GetDevice();
		CreateConstantBuffer(d, sizeof(GfxParamsPerFrame), &gpGfxPerFrame);
		CreateConstantBuffer(d, sizeof(GfxParamsPerObj), &gpGfxPerObj);
		CreateConstantBuffer(d, sizeof(GfxParamsPerMesh), &gpGfxPerMesh);
	}

	void ReleaseConstantBuffers()
	{
		ReleaseCOM(gpGfxPerFrame);
		ReleaseCOM(gpGfxPerObj);
		ReleaseCOM(gpGfxPerMesh);
	}

	void SetPerObjConsts(ID3D11DeviceContext& ctx, DirectX::SimpleMath::Matrix& world)
	{
		gGfxPerObj.world = world;
		gGfxPerObj.worldInvT = InverseTranspose(world);
		gGfxPerObj.worldViewProj = world * gView * gProj;

		ctx.UpdateSubresource(gpGfxPerObj, 0, nullptr, &gGfxPerObj, 0, 0);
	}

	void SetPerFrameConsts(ID3D11DeviceContext& ctx, const Vector3& eyePos)
	{
		gGfxPerFrame.eyePosW = Vector4(eyePos.x, eyePos.y, eyePos.z, 0);
		ctx.UpdateSubresource(gpGfxPerFrame, 0, nullptr, &gGfxPerFrame, 0, 0);

	}

	void PreRenderObj(ID3D11DeviceContext& ctx, Material& mat)
	{
		gGfxPerMesh.material = mat;
		ctx.UpdateSubresource(gpGfxPerMesh, 0, nullptr, &gGfxPerMesh, 0, 0);

		ctx.VSSetConstantBuffers(0, 1, &gpGfxPerFrame);
		ctx.VSSetConstantBuffers(1, 1, &gpGfxPerObj);
		ctx.VSSetConstantBuffers(2, 1, &gpGfxPerMesh);

		ctx.PSSetConstantBuffers(0, 1, &gpGfxPerFrame);
		ctx.PSSetConstantBuffers(1, 1, &gpGfxPerObj);
		ctx.PSSetConstantBuffers(2, 1, &gpGfxPerMesh);

	}

	void SetupDirectionalLight(int lightIdx, bool enable, const Vector3 &direction,
		const Vector3& diffuse, const Vector3& specular, const Vector3& ambient)
	{
		assert(lightIdx >= 0 && lightIdx < 8);
		if (!enable)
		{
			gGfxPerFrame.lights[lightIdx].type = Light::Type::OFF;
			return;
		}

		Light& l = gGfxPerFrame.lights[lightIdx];
		l.type = Light::Type::DIR;
		l.Diffuse = Vec3To4(diffuse, 0);
		l.Ambient = Vec3To4(ambient, 0);
		l.Specular = Vec3To4(specular, 0);
		l.Direction = Vec3To4(direction, 0);
	}

	void SetupPointLight(int lightIdx, bool enable,
		const Vector3& position,
		const Vector3& diffuse,
		const Vector3& specular,
		const Vector3& ambient,
		float range/*=1000.f*/, float atten1/*=0.05f*/)
	{
		assert(lightIdx >= 0 && lightIdx < 8);
		if (!enable)
		{
			gGfxPerFrame.lights[lightIdx].type = Light::Type::OFF;
			return;
		}

		Light& l = gGfxPerFrame.lights[lightIdx];
		l.type = Light::Type::POINT;
		l.Diffuse = Vec3To4(diffuse, 0);
		l.Ambient = Vec3To4(ambient, 0);
		l.Specular = Vec3To4(specular, 0);
		l.Position = Vec3To4(position, 0);
		l.Attenuation = Vector4(0, atten1, 0, 0);
		l.range = range;
	}

	void SetupSpotLight(int lightIdx, bool enable,
		const DirectX::SimpleMath::Vector3 &position,
		const DirectX::SimpleMath::Vector3 &direction,
		const DirectX::SimpleMath::Vector3& diffuse,
		const DirectX::SimpleMath::Vector3& specular,
		const DirectX::SimpleMath::Vector3& ambient,
		float range, float atten1, float innerConeTheta, float outerConePhi)
	{
		assert(lightIdx >= 0 && lightIdx < 8);
		if (!enable)
		{
			gGfxPerFrame.lights[lightIdx].type = Light::Type::OFF;
			return;
		}

		Light& l = gGfxPerFrame.lights[lightIdx];
		l.type = Light::Type::SPOT;
		l.Diffuse = Vec3To4(diffuse, 0);
		l.Ambient = Vec3To4(ambient, 0);
		l.Specular = Vec3To4(specular, 0);
		l.Position = Vec3To4(position, 0);
		l.Direction = Vec3To4(direction, 0);
		l.Attenuation = Vector4(0, atten1, 0, 0);
		l.range = range;
		l.theta = innerConeTheta;
		l.phi = outerConePhi;
	}


	void CheckShaderModel5Supported(ID3D11Device& d3dDevice)
	{
		D3D_FEATURE_LEVEL featureLevel = d3dDevice.GetFeatureLevel();
		bool featureLevelOK = true;
		switch (featureLevel)
		{
		case D3D_FEATURE_LEVEL_11_1:
		case D3D_FEATURE_LEVEL_11_0:
			break;
		default:
			featureLevelOK = false;
		}

		if (!featureLevelOK)
		{
			DBOUT("feature level too low for shader model 5");
			assert(false);
		}
	}

	void CreateConstantBuffer(ID3D11Device& d3dDevice, UINT sizeOfBuffer, ID3D11Buffer **pBuffer)
	{
		// Create the constant buffers for the variables defined in the vertex shader.
		D3D11_BUFFER_DESC constantBufferDesc;
		ZeroMemory(&constantBufferDesc, sizeof(D3D11_BUFFER_DESC));

		constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		constantBufferDesc.ByteWidth = sizeOfBuffer;
		constantBufferDesc.CPUAccessFlags = 0;
		constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;

		HR(d3dDevice.CreateBuffer(&constantBufferDesc, nullptr, pBuffer));

	}

	char* ReadAndAllocate(const string& fileName, unsigned int& bytesRead)
	{
		//open the file for reading
		ifstream infile;
		infile.open(fileName, ios::binary | ios::in);
		if (!infile.is_open() || infile.fail())
		{
			DBOUT("failed to open file: " << fileName);
			assert(false);
			return nullptr;
		}

		//read the whole contents
		infile.seekg(0, ios::end);
		streampos size = infile.tellg();
		if (size > INT_MAX || size <= 0)
		{
			DBOUT("failed to get size of file: " << fileName);
			assert(false);
		}
		char* pBuff = new char[(int)size];
		infile.seekg(0, ios::beg);
		infile.read(pBuff, size);
		if (infile.fail())
		{
			DBOUT("failed to read file: " << fileName);
			assert(false);
		}
		infile.close();
		bytesRead = (int)size;
		return pBuff;
	}


	void CreateInputLayout(ID3D11Device& d3dDevice, const D3D11_INPUT_ELEMENT_DESC vdesc[], int numElements, char* pBuff, unsigned int buffSz, ID3D11InputLayout** pLayout)
	{
		assert(pBuff);
		HR(d3dDevice.CreateInputLayout(vdesc, numElements, pBuff, buffSz, pLayout));

	}

	void CreateVertexShader(ID3D11Device& d3dDevice, char* pBuff, unsigned int buffSz, ID3D11VertexShader* &pVS)
	{
		assert(pBuff);
		HR(d3dDevice.CreateVertexShader(pBuff,
			buffSz,
			nullptr,
			&pVS));
		assert(pVS);
	}

	void CreatePixelShader(ID3D11Device& d3dDevice, char* pBuff, unsigned int buffSz, ID3D11PixelShader* &pPS)
	{
		assert(pBuff);
		HR(d3dDevice.CreatePixelShader(pBuff,
			buffSz,
			nullptr,
			&pPS));
		assert(pPS);
	}


	bool MyFX::Init()
	{
		CheckShaderModel5Supported(mD3D.GetDevice());

		char* pBuff = nullptr;
		unsigned int bytes = 0;
		pBuff = ReadAndAllocate("../bin/data/LightingVS.cso", bytes);
		CreateVertexShader(mD3D.GetDevice(),pBuff, bytes, mpVS);
		CreateInputLayout(mD3D.GetDevice(),VertexPosNorm::sVertexDesc, 2, pBuff, bytes, &mpInputLayout);
		delete[] pBuff;

		pBuff = ReadAndAllocate("../bin/data/LightingPSLit.cso", bytes);
		CreatePixelShader(mD3D.GetDevice(),pBuff, bytes, mpPSLit);
		delete[] pBuff;

		pBuff = ReadAndAllocate("../bin/data/LightingPSUnlit.cso", bytes);
		CreatePixelShader(mD3D.GetDevice(),pBuff, bytes, mpPSUnlit);
		delete[] pBuff;

		CreateConstantBuffers(mD3D);

		return true;
	}

	void MyFX::Release()
	{
		ReleaseCOM(mpVS);
		ReleaseCOM(mpPSLit);
		ReleaseCOM(mpPSUnlit);
		ReleaseCOM(mpInputLayout);
		ReleaseConstantBuffers();
	}

	void MyFX::Render(Model& model, unsigned int flags, Material* pOverrideMat)
	{
		assert(mpVS);
		//setup shaders
		mD3D.GetDeviceCtx().VSSetShader(mpVS, nullptr, 0);
		ID3D11PixelShader* p = mpPSUnlit;
		if (flags&FxFlags::LIT)
			p = mpPSLit;
		assert(p);
		mD3D.GetDeviceCtx().PSSetShader(p, nullptr, 0);

		Matrix w;
		model.GetWorldMatrix(w);
		SetPerObjConsts(mD3D.GetDeviceCtx(), w);

		Mesh& mesh = model.GetMesh();
		for (int i = 0; i < mesh.GetNumSubMeshes(); ++i)
		{
			//update material
			SubMesh& sm = mesh.GetSubMesh(i);

			mD3D.InitInputAssembler(mpInputLayout, sm.mpVB, sizeof(VertexPosNorm), sm.mpIB);
			Material *pM;
			if (pOverrideMat)
				pM = pOverrideMat;
			else if (model.HasOverrideMat())
				pM = model.HasOverrideMat();
			else
				pM = &sm.material;

			PreRenderObj(mD3D.GetDeviceCtx(), *pM);
			mD3D.GetDeviceCtx().DrawIndexed(sm.mNumIndices, 0, 0);
		}
	}


}

