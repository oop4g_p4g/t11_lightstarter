#include "D3D.h"
#include "SimpleMath.h"
#include "d3dutil.h"
#include "Game.h"
#include "WindowUtils.h"
#include "FX.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;


void Game::BuildQuad()
{
	// Create vertex buffer
	VertexPosNorm vertices[] =
	{	//quad in the XZ plane
		{ Vector3(-1, 0, -1), Vector3(0, 1, 0) },
		{ Vector3(-1, 0, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 0, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 0, -1), Vector3(0, 1, 0) }
	};

	// Create the index buffer
	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3
	};
	//create a mesh in the MeshMgr to hold the geometry
	Mesh &mesh = WinUtil::Get().GetD3D().GetMeshMgr().CreateMesh("quad");
	//a green quad
	Material mat(Vector4(0, 0.1f, 0, 0), Vector4(0, 1, 0, 0), Vector4(0, 1, 0, 1));
	//initialise the mesh to the geometry we just defined
	mesh.CreateFrom(vertices, 4, indices, 6, mat);
	//initialise a model to the mesh we just made
	mQuad.Initialise(mesh);
}



void Game::BuildCube()
{
	// Create vertex buffer
	VertexPosNorm vertices[] =
	{	//front
		{ Vector3(-1, -1, -1), Vector3(0, 0, -1) },
		{ Vector3(-1, 1, -1), Vector3(0, 0, -1) },
		{ Vector3(1, 1, -1), Vector3(0, 0, -1) },
		{ Vector3(1, -1, -1), Vector3(0, 0, -1) },
		//right
		{ Vector3(1, -1, -1), Vector3(1, 0, 0) },
		{ Vector3(1, 1, -1), Vector3(1, 0, 0) },
		{ Vector3(1, 1, 1), Vector3(1, 0, 0) },
		{ Vector3(1, -1, 1), Vector3(1, 0, 0) },
		//left
		{ Vector3(-1, -1, 1), Vector3(-1, 0, 0) },
		{ Vector3(-1, 1, 1), Vector3(-1, 0, 0) },
		{ Vector3(-1, 1, -1), Vector3(-1, 0, 0) },
		{ Vector3(-1, -1, -1), Vector3(-1, 0, 0) },
		//top
		{ Vector3(-1, 1, -1), Vector3(0, 1, 0) },
		{ Vector3(-1, 1, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 1, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 1, -1), Vector3(0, 1, 0) },
		//bottom
		{ Vector3(1, -1, -1), Vector3(0, -1, 0) },
		{ Vector3(1, -1, 1), Vector3(0, -1, 0) },
		{ Vector3(-1, -1, 1), Vector3(0, -1, 0) },
		{ Vector3(-1, -1, -1), Vector3(0, -1, 0) },
		//back
		{ Vector3(1, -1, 1), Vector3(0, 0, 1) },
		{ Vector3(1, 1, 1), Vector3(0, 0, 1) },
		{ Vector3(-1, 1, 1), Vector3(0, 0, 1) },
		{ Vector3(-1, -1, 1), Vector3(0, 0, 1) }
	};

	// Create the index buffer
	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3,
		//right
		4, 5, 6,
		4, 6, 7,
		//left
		8, 9, 10,
		8, 10, 11,
		//top
		12, 13, 14,
		12, 14, 15,
		//bottom
		16, 17, 18,
		16, 18, 19,
		//back
		20, 21, 22,
		20, 22, 23
	};
	//create ONE new mesh to hold this box
	Mesh &mesh = WinUtil::Get().GetD3D().GetMeshMgr().CreateMesh("box");
	Material mat(Vector4(1, 0.5, 0.5, 0), Vector4(1, 0.5, 0.5, 0), Vector4(1, 0.5, 0.5, 1));
	//initialise the mesh from our geometry data
	mesh.CreateFrom(vertices, 24, indices, 36, mat);
	//attach a model to it
	mBox.Initialise(mesh);
	//copy the model, we use it as a light bulb too
	mLight = mBox;

	//the light box we copied, use an override material to make it look a different colour to the default geometry
	mat = Material{ {0,0,0,0},{0,0,0,0},{0,0,0,0} };
	mLight.SetOverrideMat(&mat);//this is a light bulb so it just needs diffuse which will be set at run time
}

//one time set everything up
void Game::Initialise()
{
	BuildCube();
	BuildQuad();
	mBox.GetPosition() = Vector3(0, 0, 0);
	mBox.GetScale() = Vector3(1, 1, 1);

	mLight.GetScale() = Vector3(0.1f);

	mQuad.GetScale() = Vector3(3, 1, 3);
	mQuad.GetPosition() = Vector3(0, -1, 0);

	//soft dim orangey light, no specular, low ambient contribution
	FX::SetupDirectionalLight(0, true, Vector3(0.7f, -0.7f, 0.7f), Vector3(0.6f, 0.4f, 0.2f), Vector3(0, 0, 0), Vector3(0.06f, 0.05f, 0.04f));
}

void Game::Release()
{
}

void Game::Update(float dTime)
{
	mAngle += dTime * 0.5f;
	//spin the box
	mBox.GetRotation().y = mAngle;
}

void Game::Render(float dTime)
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.BeginRender(Colours::Black);

	//an oscilating value we can use
	float alpha = 0.5f + sinf(mAngle * 2)*0.5f;

	//reset the camera and projection
	CreateViewMatrix(FX::GetViewMatrix(), mCamPos, Vector3(0, 0, -2), Vector3(0, 1, 0));
	CreateProjectionMatrix(FX::GetProjectionMatrix(), 0.25f*PI, WinUtil::Get().GetAspectRatio(), 1, 1000.f);
	//tell the gpu
	FX::SetPerFrameConsts(d3d.GetDeviceCtx(), mCamPos);

	//point light using a model as a 'bulb'
	mLight.GetPosition() = Vector3(1.5f, alpha - 0.5f, -2);
	Vector3 plightColour = Vector3(alpha, 0.5f, 1.f); 
	//a coloured light where the red part of the colour keeps changing, similar level of specular, no ambient contribution
	FX::SetupPointLight(1, true, mLight.GetPosition(), plightColour, plightColour, Vector3(0, 0, 0), 15.f, 1.f);
	//tweak its override material so the colour varies in real time to match the light source - so i tlooks like the bulb
	mLight.HasOverrideMat()->Diffuse = Vec3To4(plightColour, 0);
	d3d.GetFX().Render(mLight, FX::FxFlags::NONE);


	Matrix w = Matrix::CreateRotationY(sinf(mAngle));
	FX::SetPerObjConsts(d3d.GetDeviceCtx(), w);
	//fixed downward green spot light, with specualr and no ambient
	FX::SetupSpotLight(2, true, Vector3(0, 1, -1.5), Vector3(0, -1, 0), Vector3(0.1f, 0.6f, 0.1f), Vector3(0.1f, 0.6f, 0.1f), Vector3(0, 0, 0), 10, 0.05f, D2R(1), D2R(20));

	//main cube
	//just leave the override material it was given alone
	d3d.GetFX().Render(mBox, FX::FxFlags::LIT);

	//floor - we'll tell the renderer to just ignore the material in the mesh
	//and use this local one instead, no reason, just because we can and we want it white
	//with quite a pronounced highlight
	Material mat{ {1,1,1,0},{1,1,1,0},{1,1,1,100} };
	d3d.GetFX().Render(mQuad, FX::FxFlags::LIT, &mat);

	d3d.EndRender();
}

//detect escape and exit
LRESULT Game::WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		}
	}
	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

