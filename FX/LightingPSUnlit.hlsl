#include "lighting.hlsl"

//super simple pixel shader if no lighting
float4 PSUnlit(VertexOut pin) : SV_Target
{
	return gMaterial.Diffuse;
}
  
