

#include "LightHelper.hlsl"

#define MAX_LIGHTS 3
#define LIGHT_OFF 0
#define LIGHT_DIR 1
#define LIGHT_POINT 2
#define LIGHT_SPOT 3

//constant buffer for things that change only once per frame
cbuffer cbPerFrame : register(b0)  
{
	Light gLights[MAX_LIGHTS];	//all the light data
	float4 gEyePosW;			//ignore w, it's the camera position
};

//things that change every time we render a model (object) - the matrices
cbuffer cbPerObject : register(b1)
{
	float4x4 gWorld;
	float4x4 gWorldInvTranspose;
	float4x4 gWorldViewProj;
};

//things that change from one sub-mesh to another, so just the material
cbuffer cbPerMesh : register(b2)
{
	Material gMaterial;
}

// Nonnumeric values cannot be added to a cbuffer.

//data in and out of the vertex shader
struct VertexIn
{
	//local space position and normal
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
};

struct VertexOut
{
	float4 PosH		: SV_POSITION;	//transformed homogonous space position
	float3 PosW		: POSITION;		//hang onto the world space position
	float3 NormalW	: NORMAL;		//world space normal
};

